#!/usr/bin/env bash

echo "-- Starting spark master"
echo ""
bash ${SPARK_HOME}/sbin/start-master.sh
echo ""
echo ""
echo "-- Starting spark worker"
echo ""

bash ${SPARK_HOME}/bin/spark-class org.apache.spark.deploy.worker.Worker  spark://${HOSTNAME}:7077 -c 1 -m 1500M 2> /dev/null &

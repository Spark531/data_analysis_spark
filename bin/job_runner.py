__author__ = "sushan531@gmail.com"
import os
import click
from pathlib import Path
import subprocess
import xml.etree.ElementTree as ET


python_path = Path(__file__).absolute().parent.parent
os.environ["PYTHONPATH"] = str(python_path)
spark_home = os.environ["SPARK_HOME"]


def validate_input_and_expand(input) -> list:
    """
    Validates the input to run jobs,
    If Alphanumeric char found raises exception,
    If unable to generate elongated range raises exception

    Args:
        input(str): a string of numbers seperated by either `,` or `-`
                    eg: 1,3,6-9,13,17-19

    """
    lst = []
    try:
        for char in input:
            if char.isalpha():  # checking if there is alphabets in input
                raise Exception("Alpha char found")
        input = input.split(",")

        for i in input:
            if "-" not in i:  # appending integers to new list
                lst.append(int(i))
            else:
                tmp = i.split(
                    "-"
                )  # if range type input found such as 3-9 generating a expanded list [3,4,5,6,7,8,9]
                try:
                    expanded_range = list(range(int(tmp[0]), int(tmp[-1]) + 1))
                except ValueError as e:
                    raise e
                else:
                    if len(expanded_range) != 0:
                        for j in expanded_range:
                            lst.append(j)
                    else:
                        # if input is given as 9-6 the range gives empty list raises exception
                        raise Exception(
                            "Could not get positive range from {} - {}".format(
                                tmp[0], tmp[-1]
                            )
                        )
    except (ValueError, Exception) as e:
        print("{0} >>> {1}".format(input, e))
    else:
        return lst


def create_spark_jobs_list(master, service_name) -> list:
    command_params = {
        "spark_home": spark_home,
        "master_address": master,
        "service_path": python_path.joinpath("service").joinpath(service_name),
    }
    cmd = "sh {spark_home}/bin/spark-submit --master {master_address} {service_path}.py".format_map(
        command_params
    )
    return cmd


@click.command()
@click.option("--option", type=click.Choice(["all", "list"]))
@click.option("--master", type=str, help="Address of master node")
def runner(option, master) -> None:
    """
    Reads the jobs nodes tr rows service name from xml file and generates a prompt of the jobs to run.
    Reads the user input from prompt, validates it and runs jobs
    Args:
        option(str): either all or list
        master(str): master address of spark cluster

    """
    tree = ET.parse(
        "/home/kasper/Documents/data_analysis_spark/jobs/jobs.xml"
    )

    root = tree.getroot()
    count = 0
    xml_job_list = []
    xml_job_number_list = []
    for child in root:
        count = count + 1
        tmp = (count, child.text)
        xml_job_list.append(tmp)
        xml_job_number_list.append(count)

    job_cmd_list = []
    if option == "list":
        print("\nJob Lists:\n")
        for index, job in xml_job_list:
            print("{}: {}".format(index, job))
        selc = click.prompt("\nPlease select jobs", default=None)
        input_job_list = validate_input_and_expand(selc)
        if input_job_list is not None:
            invalid_jobs = set(input_job_list).difference(xml_job_number_list)
            if len(invalid_jobs) == 0:
                for job_no in input_job_list:
                    service_name = xml_job_list[job_no - 1][1]
                    job = create_spark_jobs_list(master, service_name)
                    # job_cmd_list.append(cmd)
                    print(job)
                    subprocess.call(job, shell=True)

            else:
                print("Could not find jobs for {0} in XML file".format(invalid_jobs))

    elif option == "all":
        for job_no, service_name in xml_job_list:
            cmd = create_spark_jobs_list(master, service_name)
            job_cmd_list.append(cmd)
        for job in job_cmd_list:
            print(job)
            subprocess.call(job, shell=True)


if __name__ == "__main__":
    runner()
    exit()

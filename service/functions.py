__author__ = "sushan531@gmail.com"
import sys
from utils import dump_to_file
from utils.config import SparkPaths
from service import AbstractClusterConnectSpark


# this function inside the class gave error so had to define outside the scope of class
def cleanse_rdd(line) -> str:
    """
    Function to clean each line of RDD
    Args:
        line(str): a row of RDD

    Returns:
        rowafter cleaning the data

    """
    if isinstance(line, int):
        return line
    att_list = line.split(",")
    if att_list[3] == "two":
        att_list[3] = "2"
    else:
        att_list[3] = "4"
    att_list[5] = att_list[5].upper()
    return ",".join(att_list)


# this function inside the class gave error so had to define outside the scope of class
def get_miles_per_gallon(line) -> int:
    """
    Function to get the miles per gallon for each line of RDD
    Args:
        line(str): a row of RDD

    Returns:
        The miles per gallon of index 9 if splitted list

    """
    if isinstance(line, int):
        return line
    att_list = line.split(",")
    if att_list[9].isdigit():
        return int(att_list[9])
    else:
        return 0


def functions(data_file) -> None:
    """
    Practise excercize on how to use functions for reduce
    Args:
        data_file(str): file name to perform spark operations on

    """
    sp_obj = SparkPaths()
    acc_obj = AbstractClusterConnectSpark("DA_Functions")
    acc_obj.data_file = sp_obj.data_dir.joinpath(data_file)
    acc_obj.output_file = sp_obj.output_dir.joinpath("functions")
    auto_data = acc_obj.sc.textFile(str(acc_obj.data_file))
    auto_data.cache()  # auto_data wont be garbage after completion, it will persist in memory due to cache function

    # for every line in auto_data the cleanse_rdd is run in almost parallel fashion
    cleaned_data = auto_data.map(cleanse_rdd)
    dump_to_file(cleaned_data.collect(), acc_obj.output_file)

    mpg_for_all = (
        auto_data.reduce(lambda x, y: get_miles_per_gallon(x) + get_miles_per_gallon(y))
        / auto_data.count()
        - 1
    )
    dump_to_file(
        "Miles per gallon for all the data is: {}\n".format(mpg_for_all),
        acc_obj.output_file,
    )
    # taking only brand name and HP of the vehicle into new rdd
    cyl_data = auto_data.map(lambda x: (x.split(",")[0], x.split(",")[7]))

    # removing header
    header = cyl_data.first()
    cyl_data_hp = cyl_data.filter(lambda line: line != header)

    # totalling the Hp value of each brand and counting the occurance of the brand
    # gives ("brand",(total_hp, brand_count))
    # then the reducebykey will give ("brand", average_hp=( total_hp / brand_count))
    total_hp_values_brandcount = cyl_data_hp.mapValues(lambda x: (x, 1)).reduceByKey(
        lambda x, y: (int(x[0]) + int(x[0]), y[1] + y[1])
    )

    total_hp = total_hp_values_brandcount.collect()
    dump_to_file(total_hp, acc_obj.output_file)
    average_hp = total_hp_values_brandcount.mapValues(lambda x: int(x[0]) / int(x[1]))
    average = average_hp.collect()
    dump_to_file(average, acc_obj.output_file)

    sedancount = acc_obj.sc.accumulator(0)
    hatchbackcount = acc_obj.sc.accumulator(0)

    sedantext = acc_obj.sc.broadcast("sedan")
    hatchbacktext = acc_obj.sc.broadcast("hatch")

    def accumulate_and_use_broadcast(line, sedancount, hatchbackcount):
        if sedantext.value in line:
            sedancount += 1
        if hatchbacktext.value in line:
            hatchbackcount += 1

    auto_data.map(
        lambda line: accumulate_and_use_broadcast(line, sedancount, hatchbackcount)
    ).count()

    dump_to_file(
        "SEDAN_COUNT:{}, HATCHBACK_COUNT:{}".format(sedancount, hatchbackcount),
        acc_obj.output_file,
    )


if __name__ == "__main__":
    if len(sys.argv) != 1:
        sys.stdout.write("Usage: wordcount <file>", file=sys.stderr)
        sys.exit(-1)

    functions("auto-data.csv")

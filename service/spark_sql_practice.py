__author__ = "sushan531@gmail.com"
from utils.config import SparkPaths
from service import AbstractClusterConnectSql


def spark_sql_practice(data_file) -> None:
    """
    Practise excercize on how to use functions for reduce
    Args:
        data_file(str): file name to perform spark operations on

    """
    sp_obj = SparkPaths()
    acc_obj = AbstractClusterConnectSql("DA_sparksql")
    acc_obj.data_file = sp_obj.data_dir.joinpath(data_file)
    acc_obj.output_file = sp_obj.output_dir.joinpath("spark_sql_practice")
    empdf = acc_obj.sc.read.json(str(acc_obj.data_file))
    empdf.show()
    empdf.printSchema()


if __name__ == "__main__":
    spark_sql_practice("customerData.json")

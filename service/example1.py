__author__ = "sushan531@gmail.com"
import sys
from utils import dump_to_file
from utils.config import SparkPaths
from service import AbstractClusterConnectSpark


def example1(data_file):
    sp_obj = SparkPaths()
    acc_obj = AbstractClusterConnectSpark("DA_Example1")
    acc_obj.data_file = sp_obj.data_dir.joinpath(data_file)
    acc_obj.output_file = sp_obj.output_dir.joinpath("example1")
    auto_data = acc_obj.sc.textFile(str(acc_obj.data_file))
    auto_data.cache()  # auto_data wont be garbage after completion, it will persist in memory due to cache function
    dump_to_file(
        "Auto data count :\n{}\n".format(auto_data.count()), acc_obj.output_file
    )
    dump_to_file(
        "Auto data first line :\n{}\n".format(auto_data.first()), acc_obj.output_file
    )
    dump_to_file(
        "Auto data first 5 line :\n{}\n".format(auto_data.take(5)), acc_obj.output_file
    )

    dump_to_file("Printing line by line after COLLECT action.", acc_obj.output_file)
    for line in auto_data.collect():
        dump_to_file(line, acc_obj.output_file)

    # Loading Data From a Collection
    # consider the datais in hdfs.
    # in hdfs the data is stored in distributed format so we need to collect data using PARALLELIZE action
    coll_data = acc_obj.sc.parallelize([3, 5, 4, 7, 4])
    coll_data.cache()
    dump_to_file(
        "Coll data count :\n{}\n".format(coll_data.count()), acc_obj.output_file
    )

    # Map and create a new RDD
    # here for every line in the auto_data, code replaces `,` with `tab`
    tsv_data = auto_data.map(lambda x: x.replace(",", "\t"))
    dump_to_file(
        "First 5 values of tab separated data :\n{}\n".format(tsv_data.take(5)),
        acc_obj.output_file,
    )

    # Filter and create new RDD
    toyota_data = auto_data.filter(lambda x: "toyota" in x)
    dump_to_file(
        "Total toyota vehicles :\n{}\n".format(toyota_data.count()), acc_obj.output_file
    )

    # Flat map example
    # unlike MAP ing for a single line, FLATMAP runs the code throughout the while RDD.
    words = auto_data.flatMap(lambda whole_data: whole_data.split(","))
    dump_to_file(
        "total words in the data is :\n{}\n".format(words.take(20)), acc_obj.output_file
    )

    # distinct data
    dump_to_file("Distinct data :", acc_obj.output_file)
    for disct_data in coll_data.distinct().collect():
        dump_to_file(disct_data, acc_obj.output_file)

    # set operations
    words1 = acc_obj.sc.parallelize(["hello", "war", "peace", "world"])
    words2 = acc_obj.sc.parallelize(["war", "peace", "universe"])

    # example of lazy operation
    # using the collect action will trigger all the previous operation
    union_unique = words1.union(words2).distinct().collect()
    intersect = words1.intersection(words2).collect()

    dump_to_file("union_unique :{}".format(union_unique), acc_obj.output_file)
    dump_to_file("intersection :{}".format(intersect), acc_obj.output_file)

    # reduce
    dump_to_file(
        "Sum of the coll_data:{}".format(coll_data.reduce(lambda x, y: x + y)),
        acc_obj.output_file,
    )

    # find the shortest line
    shortest_line = auto_data.reduce(lambda x, y: x if len(x) < len(y) else y)
    dump_to_file("Shortest line : {}".format(shortest_line), acc_obj.output_file)

    # aggregate example 0
    # col_data = [3, 5, 4, 7, 4]
    # sequence operation happens to element operation in partition
    # i.e for each element in partition seq_op is p1:0+3, p2:0+5 and so on
    def seq_op(x, y):
        # (x = 0)
        return x + y

    # combine operation happens for all the partitions
    # i.e for each partition comb_op is (((p1+p2)+p3)+p4)+..so on
    def comb_op(x, y):
        return x + y

    coll_data.aggregate(0, seq_op, comb_op)

    # aggregate example 1
    # in the below aggregate we are passing a tuple as zerovalue, thus we are using x[0]=2, x[1]=3
    # this sequence operation returns p1:(2+3, 3*3) p2:(2+5, 3*5)
    seq_op = lambda x, y: (x[0] + y, x[1] * y)

    # the result of seq op is combines as (((p1[0] + p2[0]) + p3[0]) + so on.. , ((p1[0] * p2[0]) * p3[0])* so on..)
    comb_op = lambda x, y: (x[0] + y[0], x[1] * y[1])

    coll_data.aggregate((2, 3), seq_op, comb_op)


if __name__ == "__main__":
    if len(sys.argv) != 1:
        sys.stdout.write("Usage: wordcount <file>", file=sys.stderr)
        sys.exit(-1)

    example1("auto-data.csv")

from utils.connect import SparkConnectMaster


class AbstractClusterConnectSpark(SparkConnectMaster):
    def __init__(self, app_name):
        super().__init__()
        self.data_file = ""
        self.APP_NAME = app_name
        self.sc = self.connect_master_spark(self.APP_NAME)
        self.output_file = ""


class AbstractClusterConnectSql(SparkConnectMaster):
    def __init__(self, app_name):
        super().__init__()
        self.data_file = ""
        self.APP_NAME = app_name
        self.sc = self.connect_master_sql(self.APP_NAME)
        self.output_file = ""

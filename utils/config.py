__author__ = "sushan531@gmail.com"
import configparser
from utils.path import SparkPaths


class Config:
    def __init__(self):
        self.configs = self.read_config_file()

    @staticmethod
    def read_config_file() -> configparser:
        """
        Reads the config.ini file and returns configparser object
        """
        root_path = SparkPaths.init_root_dir()
        config_file = root_path.joinpath("config").joinpath("config.ini")
        spark_config = ""
        if config_file.is_file():
            spark_config = configparser.ConfigParser()
            spark_config.read(config_file)

        return spark_config

    def get_config_value(self, section, key) -> str:
        """
        Returns the value of key from a specified section of config.ini file object
        Args:
            section(str): section of config.ini
            key(str): key of get value from
        """
        return self.configs.get(section, key)

__author__ = "sushan531@gmail.com"
from pathlib import Path


class SparkPaths:
    def __init__(self):
        self.root_path = self.init_root_dir()
        self.data_dir = self.root_path.joinpath("data")
        self.output_dir = self.root_path.joinpath("output")
        self.common_const_dir = self.root_path.joinpath("common")
        self.service_const_dir = self.common_const_dir.joinpath("service")

    @staticmethod
    def init_root_dir():
        """
        Returns the root directory of the project.
        """
        return Path(__file__).parent.parent

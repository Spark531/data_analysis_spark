__author__ = "sushan531@gmail.com"
from common.constants.config_constants import *
from utils.config import Config

from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession


class SparkConnectMaster:
    def __init__(self):
        self.conf = ""
        self.spark_config = ""

    def spark_conf(self, app_name) -> SparkConf:
        """
            sets required values to the object of SparkConf data type
        Args:`
            app_name(str): name of the job

        """
        self.conf = SparkConf()
        self.spark_config = Config()
        self.conf.setMaster(self.spark_config.get_config_value(CONFIG_SECTION, MASTER))
        self.conf.setAppName(app_name)
        self.conf.set(
            "spark.executor.memory",
            self.spark_config.get_config_value(CONFIG_SECTION, MEMORY),
        )
        self.conf.set(
            "spark.cores.max", self.spark_config.get_config_value(CONFIG_SECTION, CORES)
        )
        return self.conf

    def connect_master_spark(self, app_name) -> SparkContext:
        """
        returns a object of connection to master node
        Args:
            app_name(str): name of job

        """
        return SparkContext(conf=self.spark_conf(app_name))

    def connect_master_sql(self, app_name) -> SparkSession:
        """
        returns a object of connection to master node
        Args:
            app_name(str): name of job

        """
        return SparkSession(sparkContext=SparkContext(conf=self.spark_conf(app_name)))

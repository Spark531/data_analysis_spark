__author__ = "sushan531@gmail.com"


def dump_to_file(content, file_path, truncate=False):
    mode = "w+" if truncate else "a"
    with open(file_path, mode=mode) as obj:
        obj.write("{0}".format(content))
